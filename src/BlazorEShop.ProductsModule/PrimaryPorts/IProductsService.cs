﻿using System.Threading.Tasks;
using BlazorEShop.ProductsModule.Dto;
using BlazorEShop.ProductsModule.Entities;
using BlazorEShop.SharedModule;

namespace BlazorEShop.ProductsModule.PrimaryPorts
{
    public interface IProductsService
    {
        Task<int> Add(Product request);
        Task<PageResult<ConcurrencyToken<Product>>> Get(ProductsFilter request);
        Task<int> Remove(ConcurrencyToken<ProductId> request);
        Task<int> Update(ConcurrencyToken<Product> request);
    }
}
