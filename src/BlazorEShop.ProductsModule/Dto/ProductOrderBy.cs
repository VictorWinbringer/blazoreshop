﻿namespace BlazorEShop.ProductsModule.Dto
{
    public enum ProductOrderBy
    {
        Id = 10,
        Name,
        Price
    }
}
