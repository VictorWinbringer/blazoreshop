﻿using BlazorEShop.SharedModule.Entities;
using FluentValidationGuard;

namespace BlazorEShop.ProductsModule.Entities
{
    public readonly struct Title
    {
        public string Value { get; }

        public Title(string value)
        {
            Validator.Begin(value, nameof(value))
                .NotNull()
                .NotWhiteSpace()
                .ThrowApiException(nameof(Title), nameof(Title));
            Value = value;
        }
    }
}
