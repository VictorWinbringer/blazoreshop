﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorEShop.ProductsModule.Dto;
using BlazorEShop.ProductsModule.Entities;
using BlazorEShop.SharedModule;

namespace BlazorEShop.ProductsModule.SecondaryPorts
{
    public interface IProductsRepository
    {
        Task<List<ConcurrencyToken<Product>>> Get(ProductsFilter productsFilter);
        void Add(Product product);
        Task Update(ConcurrencyToken<Product> product);
        Task Remove(ConcurrencyToken<ProductId> id);
        Task<ConcurrencyToken<Product>> Get(ProductId id);
        Task<int> Count(ProductsFilter productsFilter);
    }
}
