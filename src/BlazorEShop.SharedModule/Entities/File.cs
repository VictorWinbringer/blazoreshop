﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using FluentValidationGuard;

namespace BlazorEShop.SharedModule.Entities
{
    public struct FileHash
    {
        public FileHash(byte[] value)
        {
            Value = value;
        }

        public byte[] Value { get; }

        public override string ToString() => Value == null ? null : BitConverter.ToString(Value)?.Replace("-", "");
    }

    public struct File
    {
        public Stream Data { get; }

        public FileHash ComputeFileHash()
        {
            Data.Seek(0, SeekOrigin.Begin);
            using var md5 = MD5.Create();
            return new FileHash(md5.ComputeHash(Data));
        }

        public long Size => Data.Length;

        public File(Stream data)
        {
            Validator.Begin(data, nameof(data))
                .NotNull()
                .ThrowApiException(nameof(File), nameof(File));
            Data = data;
        }


      private readonly static byte[] chkBytejpg = { 255, 216, 255, 224 };
      private static readonly byte[] chkBytebmp = { 66, 77 };
      private readonly static byte[] chkBytegif = { 71, 73, 70, 56 };
      private readonly static byte[] chkBytepng = { 137, 80, 78, 71 };

        public bool IsImage()
        {
            byte[] bytFile = new byte[4];
            Data.Seek(0, SeekOrigin.Begin);
            Data.Read(bytFile, 0, bytFile.Length);
            return EqBytes(chkBytejpg) ||
                   EqBytes(chkBytebmp) ||
                   EqBytes(chkBytegif) ||
                   EqBytes(chkBytepng);
            bool EqBytes(byte[] bytes) => bytes.Zip(bytFile, (a, b) => a == b).All(x => x);
        }
    }
}
