﻿using System.Threading.Tasks;
using BlazorEShop.SharedModule.Entities;

namespace BlazorEShop.SharedModule.SecondaryPorts
{
    public interface IFilesRepository
    {
        Task<FileHash> Upload(File file);
        Task<File> Download(FileHash hash);
    }
}
