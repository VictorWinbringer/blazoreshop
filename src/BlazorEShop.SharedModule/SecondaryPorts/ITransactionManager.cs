﻿using System;
using System.Threading.Tasks;

namespace BlazorEShop.SharedModule.SecondaryPorts
{
    public interface ITransactionManager : IDisposable
    {
        Task Commit();
        Task RollBack();
    }
}
