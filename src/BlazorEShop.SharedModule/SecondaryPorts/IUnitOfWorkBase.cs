﻿using System.Threading.Tasks;

namespace BlazorEShop.SharedModule.SecondaryPorts
{
    public interface IUnitOfWorkBase
    {
        Task<int> SaveAsync();
    }
}
