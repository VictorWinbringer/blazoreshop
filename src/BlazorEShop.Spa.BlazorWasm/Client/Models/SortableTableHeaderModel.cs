﻿using System.Collections.Generic;

namespace BlazorEShop.Spa.BlazorWasm.Client.Models
{
    public sealed class SortableTableHeaderModel
    {
        public bool IsActive { get; set; }
        public bool Descending { get; set; }
    }
}