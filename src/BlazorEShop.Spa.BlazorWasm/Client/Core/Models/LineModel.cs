﻿namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Models
{
    public class LineModel
    {
        public uint Quantity { get; set; }
        public ProductModel Product { get; set; }
    }
}