﻿using System;

namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Models
{
    public sealed class ProductModel
    {
        public Guid Id { get; set; }
        public string Version { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public Guid ImageId { get; set; }
    }
}