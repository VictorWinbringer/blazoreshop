﻿using System.Collections.Generic;

namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Models
{
    public class BasketModel
    {
        public List<LineModel> Lines { get; set; } = new List<LineModel>();
    }
}
