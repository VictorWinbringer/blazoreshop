﻿namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Models
{
    public enum OrderStatus
    {
        Created = 10,
        Delivered,
    }
}