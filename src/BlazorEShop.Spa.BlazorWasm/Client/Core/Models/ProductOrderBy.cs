﻿namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Models
{
    public enum ProductOrderBy
    {
        Id = 10,
        Title,
        Price
    }
}