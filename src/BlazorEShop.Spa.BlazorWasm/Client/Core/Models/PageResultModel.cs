﻿using System.Collections.Generic;

namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Models
{
    public sealed class PageResultModel<T>
    {
        public int TotalCount { get; set; }
        public List<T> Value { get; set; } = new List<T>();
    }
}