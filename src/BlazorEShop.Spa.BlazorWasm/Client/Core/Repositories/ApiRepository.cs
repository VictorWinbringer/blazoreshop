﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorEShop.Contracts;
using BlazorEShop.Spa.BlazorWasm.Client.Core.HttpContext;
using BlazorEShop.Spa.BlazorWasm.Client.Core.Models;
using BlazorEShop.Spa.BlazorWasm.Client.ViewModels;

namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Repositories
{
    public sealed class ApiRepository : IApiRepository
    {
        private readonly ConfigModel _config;
        private readonly IHttpContext _http;

        public ApiRepository(ConfigModel config, IHttpContext http)
        {
            _config = config;
            _http = http;
        }

        public Task<(PageResultModel<ProductModel>, string)> GetFiltered(ProductsFilterModel model)
        {
            var url = GetFullUrl("/products/filter");
            return _http.PostAsync<PageResultModel<ProductModel>>(url, model, false);
        }

        public Task<(int, string)> AddToBasket(ProductModel product)
        {
            var url = GetFullUrl($"/basket/lines/add");
            return _http.PostAsync<int>(url, product);
        }

        public Task<(BasketModel, string)> GetBasket()
        {
            var url = GetFullUrl("/basket");
            return _http.GetAsync<BasketModel>(url);
        }

        public Task<(int, string)> Remove(Guid id)
        {
            var url = GetFullUrl($"basket/lines/{id}/remove");
            return _http.PostAsync<int>(url, "");
        }

        public Task<(int, string)> ClearBasket()
        {
            var url = GetFullUrl("basket/lines");
            return _http.DeleteAsync<int>(url);
        }

        public Task<(Guid, string)> CreateOrder(IEnumerable<LineModel> lines, string address)
        {
            var url = GetFullUrl("orders");
            return _http.PostAsync<Guid>(url, new { lines, address });
        }

        public Task<(List<OrderModel>, string)> GetOrders()
        {
            var url = GetFullUrl("orders");
            return _http.GetAsync<List<OrderModel>>(url);
        }

        public string GetFullUrl(string path)
        {
            var uri = _config.ApiUri;
            uri = (uri.EndsWith('/') ? uri : uri + "/");
            path = path.StartsWith('/') ? path : ("/" + path);
            return $"{uri}api/v1{path}";
        }
    }
}
