﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorEShop.Spa.BlazorWasm.Client.Core.Models;

namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Services
{
    public interface IProductsService
    {
        string Error { get; }
        IReadOnlyList<ProductModel> Model { get; }
        int TotalCount { get; }
        Task Load(ProductsFilterModel filter);
        string ImageUri(ProductModel product);
    }
}