﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorEShop.Spa.BlazorWasm.Client.Core.Models;

namespace BlazorEShop.Spa.BlazorWasm.Client.Core.Services
{
    public interface IOrderService
    {
        string Error { get; }
        IReadOnlyList<OrderModel> Orders { get; }
        Task Create(IEnumerable<LineModel> lines, string address);
        Task Load();
    }
}