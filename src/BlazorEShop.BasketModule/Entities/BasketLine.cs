﻿using BlazorEShop.SharedModule.Entities;

namespace BlazorEShop.BasketModule.Entities
{
    public class BasketLine : Line<BasketProduct>
    {

        public BasketLine(BasketProduct basketProduct, uint quantity) : base(basketProduct, quantity)
        {
        }
    }
}
