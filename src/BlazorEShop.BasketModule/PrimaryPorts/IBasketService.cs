﻿using System;
using System.Threading.Tasks;
using BlazorEShop.BasketModule.Entities;

namespace BlazorEShop.BasketModule.PrimaryPorts
{
    public interface IBasketService
    {
        Task<int> AddProduct(BasketId id, BasketProduct basketProduct);
        Task<int> Clear(BasketId id);
        Task<Basket> Get(BasketId id);
        Task<int> RemoveProduct(BasketId id, Guid productId);
    }
}
