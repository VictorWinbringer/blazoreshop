﻿namespace BlazorEShop.BasketModule.SecondaryPorts
{
    public interface IConfigRepository
    {
        int GetMaxBasketSize();
    }
}
