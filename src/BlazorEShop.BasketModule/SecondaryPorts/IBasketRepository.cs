﻿using System.Threading.Tasks;
using BlazorEShop.BasketModule.Entities;

namespace BlazorEShop.BasketModule.SecondaryPorts
{
    public interface IBasketRepository
    {
        Task<Basket> Get(BasketId id);
        Task Save(Basket basket);
    }
}
