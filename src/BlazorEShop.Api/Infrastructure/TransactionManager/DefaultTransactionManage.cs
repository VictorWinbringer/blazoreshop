﻿using System.Threading.Tasks;
using BlazorEShop.SharedModule.SecondaryPorts;
using Microsoft.EntityFrameworkCore.Storage;

namespace BlazorEShop.Api.Infrastructure.TransactionManager
{
    internal sealed class DefaultTransactionManage : ITransactionManager
    {
        private readonly IDbContextTransaction _transaction;

        public DefaultTransactionManage(IDbContextTransaction transaction)
        {
            _transaction = transaction;
        }

        public void Dispose()
        {
            _transaction.Dispose();
        }

        public Task Commit()
        {
            return _transaction.CommitAsync();
        }

        public Task RollBack()
        {
            return _transaction.RollbackAsync();
        }
    }
}
