﻿using BlazorEShop.Api.EF;
using BlazorEShop.ImagesModule.SecondaryPorts;

namespace BlazorEShop.Api.Infrastructure.UnitsOfWork
{
    internal sealed class ImagesUow : UnitOfWorkBase, IImagesUow
    {
        public ImagesUow(EShopContext context) : base(context)
        {
        }
    }
}
