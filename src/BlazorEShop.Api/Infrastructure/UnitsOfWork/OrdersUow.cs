﻿using BlazorEShop.Api.EF;
using BlazorEShop.OrdersModule.SecondaryPorts;

namespace BlazorEShop.Api.Infrastructure.UnitsOfWork
{
    internal sealed class OrdersUow : UnitOfWorkBase, IOrdersUow
    {
        public OrdersUow(EShopContext context) : base(context)
        {
        }
    }
}
