﻿using BlazorEShop.Api.EF;
using BlazorEShop.ProductsModule.SecondaryPorts;

namespace BlazorEShop.Api.Infrastructure.UnitsOfWork
{
    internal sealed class ProductsUow : UnitOfWorkBase, IProductsUow
    {
        public ProductsUow(EShopContext context, IProductsRepository products) : base(context)
        {
            Products = products;
        }

        public IProductsRepository Products { get; }
    }
}
