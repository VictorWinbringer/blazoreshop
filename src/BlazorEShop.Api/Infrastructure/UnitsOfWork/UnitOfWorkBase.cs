﻿using System.Threading.Tasks;
using BlazorEShop.Api.EF;
using BlazorEShop.SharedModule.SecondaryPorts;

namespace BlazorEShop.Api.Infrastructure.UnitsOfWork
{
    internal abstract class UnitOfWorkBase : IUnitOfWorkBase
    {
        private readonly EShopContext _context;

        protected UnitOfWorkBase(EShopContext context)
        {
            _context = context;
        }

        public Task<int> SaveAsync()
        {
            return _context.SaveChangesAsync();
        }
    }
}
