﻿using BlazorEShop.Api.EF;
using BlazorEShop.BasketModule.SecondaryPorts;

namespace BlazorEShop.Api.Infrastructure.UnitsOfWork
{
    internal sealed class BasketUow : UnitOfWorkBase, IBasketUow
    {
        public BasketUow(EShopContext context) : base(context)
        {
        }
    }
}
