﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorEShop.Api.EF;
using BlazorEShop.Api.EF.Dto;
using BlazorEShop.ImagesModule.Entities;
using BlazorEShop.ImagesModule.SecondaryPorts;
using BlazorEShop.SharedModule.Entities;
using Microsoft.EntityFrameworkCore;

namespace BlazorEShop.Api.Infrastructure.Repositories
{
    internal sealed class ImagesRepository : IImagesRepository
    {

        private readonly EShopContext _context;

        public ImagesRepository(EShopContext context)
        {
            _context = context;
        }

        public async Task Add(ImageInfo file)
        {
            _context.Images.Add(new ImageDto(file));
        }

        public async Task<Guid> Find(FileHash hash)
        {
            var file = await _context.Images.FirstOrDefaultAsync(f => f.Hash == hash.Value);
            return file?.Id ?? default;
        }

        public async Task<ImageInfo> Get(Guid id)
        {
            var file = await _context.Images.FindAsync(id);
            return file?.ToInfo();
        }

        public async Task<List<ImageInfo>> GetAll()
        {
            var files = await _context.Images.ToListAsync();
            return files.Select(f => f.ToInfo()).ToList();
        }
    }
}
