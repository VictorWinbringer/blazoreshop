﻿using System.IO;
using System.Threading.Tasks;
using BlazorEShop.Api.AspNet.Settings;
using BlazorEShop.SharedModule.Entities;
using BlazorEShop.SharedModule.SecondaryPorts;
using File = BlazorEShop.SharedModule.Entities.File;

namespace BlazorEShop.Api.Infrastructure.Repositories
{
    internal sealed class FilesRepository : IFilesRepository
    {
        private readonly IConfig _config;

        public FilesRepository(IConfig config)
        {
            _config = config;
        }

        public async Task<FileHash> Upload(File file)
        {
            var hash = file.ComputeFileHash();
            var path = GetPath(hash);
            if (System.IO.File.Exists(path))
                return hash;
            using var stream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.None, bufferSize: 4_000_000);
            file.Data.Seek(0, SeekOrigin.Begin);
            await file.Data.CopyToAsync(stream);
            await stream.FlushAsync();
            return hash;
        }

        public async Task<File> Download(FileHash hash)
        {
            var path = GetPath(hash);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4_000_000);
            return new File(stream);

        }
        private string GetPath(FileHash hash) => Path.Combine(_config.FilesDir, hash.ToString());
    }
}
