﻿using BlazorEShop.Api.AspNet.Settings;
using BlazorEShop.BasketModule.SecondaryPorts;

namespace BlazorEShop.Api.Infrastructure.Repositories
{
    internal sealed class ConfigRepository : IConfigRepository
    {
        private readonly IConfig _config;

        public ConfigRepository(IConfig config)
        {
            _config = config;
        }

        public int GetMaxBasketSize() => _config.MaxBasketSize;
    }
}
