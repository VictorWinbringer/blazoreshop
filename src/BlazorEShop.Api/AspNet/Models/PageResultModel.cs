﻿using System.Collections.Generic;
using BlazorEShop.SharedModule;

namespace BlazorEShop.Api.AspNet.Models
{
    public sealed class PageResultModel<T>
    {
        public PageFilter ProductsFilter { get; set; }
        public uint TotalCount { get; set; }
        public List<T> Value { get; set; }
    }
}
