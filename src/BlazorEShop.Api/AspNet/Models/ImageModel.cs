﻿using System;
using BlazorEShop.ImagesModule.Entities;

namespace BlazorEShop.Api.AspNet.Models
{
    public sealed class ImageModel
    {
        public Guid Id { get; set; }
        public byte[] Hash { get; set; }
        public long Size { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public string ContentType { get; set; }

        public ImageModel()
        {
            //For framework
        }

        public ImageModel(ImageInfo @base)
        {
            Id = @base.Id;
            Hash = @base.Hash.Value;
            Size = @base.Size;
            Name = @base.Name;
            Created = @base.Created;
            ContentType = @base.ContentType;
        }
    }
}
