﻿using System.Collections.Generic;
using FluentValidationGuard;

namespace BlazorEShop.Api.AspNet.Models
{
    public sealed class ErrorModel
    {
        public string TraceIdentifier { get; set; }
        public string Path { get; set; }
        public List<Error> Errors { get; set; }
    }
}
