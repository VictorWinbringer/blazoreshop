﻿using BlazorEShop.ProductsModule.Dto;

namespace BlazorEShop.Api.AspNet.Models
{
    public sealed class PageModel
    {
        public uint Take { get; set; }
        public uint Skip { get; set; }
        public string Title { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public bool Descending { get; set; }
        public ProductOrderBy OrderBy { get; set; }
    }
}
