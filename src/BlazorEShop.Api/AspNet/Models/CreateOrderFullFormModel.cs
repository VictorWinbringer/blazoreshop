﻿using System.Collections.Generic;

namespace BlazorEShop.Api.AspNet.Models
{
    public sealed class CreateOrderFullFormModel : CreateOrderShortModel
    {
        public List<LineModel> Lines { get; set; } = new List<LineModel>();
    }
}
