﻿using System;
using System.Collections.Generic;
using System.IO;
using BlazorEShop.Api.AspNet.Helpers;
using BlazorEShop.Api.AspNet.Settings;
using BlazorEShop.Api.EF;
using BlazorEShop.Api.Infrastructure.Repositories;
using BlazorEShop.Api.Infrastructure.UnitsOfWork;
using BlazorEShop.BasketModule.PrimaryAdapters;
using BlazorEShop.BasketModule.PrimaryPorts;
using BlazorEShop.BasketModule.SecondaryPorts;
using BlazorEShop.ImagesModule.PrimaryAdapters;
using BlazorEShop.ImagesModule.PrimaryPorts;
using BlazorEShop.ImagesModule.SecondaryPorts;
using BlazorEShop.OrdersModule.PrimaryAdapters;
using BlazorEShop.OrdersModule.PrimaryPorts;
using BlazorEShop.OrdersModule.SecondaryPorts;
using BlazorEShop.ProductsModule.Entities;
using BlazorEShop.ProductsModule.PrimaryAdapters;
using BlazorEShop.ProductsModule.PrimaryPorts;
using BlazorEShop.ProductsModule.SecondaryPorts;
using BlazorEShop.SharedModule.SecondaryPorts;
using FluentValidationGuard;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.OpenApi.Models;
using File = BlazorEShop.SharedModule.Entities.File;

namespace BlazorEShop.Api.AspNet
{
    internal sealed class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var config = new ConfigDto();
            Configuration.Bind(config);
            config.FilesDir = Path.Combine(Environment.WebRootPath, "files");
            services.AddSingleton<IConfig>(p => config);
            services.AddControllers();
            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = config.Authority;
                    options.Audience = config.ApiName;
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters.ValidateIssuer = false;
                });

            services.AddCors(options =>
            {
                options.AddPolicy("default", policy =>
                {
                    policy.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "EShop API", Version = "v1" });
                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows()
                    {
                        Implicit = new OpenApiOAuthFlow()
                        {
                            AuthorizationUrl = new Uri($"{config.IdentityUrlExternal}/connect/authorize"),
                            TokenUrl = new Uri($"{config.IdentityUrlExternal}/connect/token"),
                            Scopes = new Dictionary<string, string>()
                            {
                                { "api", "BlazorEShop API" }
                            }
                        }
                    }
                });

                c.OperationFilter<AuthorizeCheckOperationFilter>();

            });

            services.AddDbContextPool<EShopContext>(builder =>
                builder.UseNpgsql(config.ConnectionString));
            services.AddHttpContextAccessor();
            services.AddTransient<IOrdersUow, OrdersUow>();
            services.AddTransient<IBasketUow, BasketUow>();
            services.AddTransient<IProductsUow, ProductsUow>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IBasketRepository, BasketRepository>();
            services.AddTransient<IProductsRepository, ProductRepository>();
            services.AddTransient<IProductsService, ProductsService>();
            services.AddTransient<IBasketService, BasketService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IErrorConverter, ErrorConverter>();
            services.AddTransient<IImagesUow, ImagesUow>();
            services.AddTransient<IFilesRepository, FilesRepository>();
            services.AddTransient<IImagesService, ImagesService>();
            services.AddTransient<IConfigRepository, ConfigRepository>();
            services.AddTransient<IImagesRepository, ImagesRepository>();
            IdentityModelEventSource.ShowPII = true;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider sp)
        {
            using (var scope = sp.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<EShopContext>();
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
                var files = scope.ServiceProvider.GetRequiredService<IImagesService>();
                var items = scope.ServiceProvider.GetRequiredService<IProductsService>();
                for (int i = 1; i < 10; i++)
                {
                    using (var stream = System.IO.File.OpenRead(Path.Combine(Environment.WebRootPath, $"{i}.png")))
                    {
                        var imageId = files.Upload(new File(stream), $"image{i}.png", "image/png").Result;
                        var _ = items.Add(new Product(new Title($"image{i}"), new Money(i), new Image(imageId))).Result;
                    }
                }
            }
            app.UseForwardedHeaders();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "EShop API V1");
                c.RoutePrefix = string.Empty;
                c.OAuthClientId("apiSwaggerUi");
                c.OAuthAppName("EShop Swagger UI");
            });
            app.UseExceptionHandler("/api/v1/error");
            app.UseRouting();
            app.UseCors("default");
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
