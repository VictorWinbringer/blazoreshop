﻿using System.Security.Claims;

namespace BlazorEShop.Api.AspNet.Helpers
{
    public static class UserParser
    {
        public static string GetId(this ClaimsPrincipal user) => user.FindFirst(ClaimTypes.NameIdentifier).Value;
    }
}
