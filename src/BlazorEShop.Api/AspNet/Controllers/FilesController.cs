﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorEShop.Api.AspNet.Models;
using BlazorEShop.ImagesModule.PrimaryPorts;
using BlazorEShop.SharedModule.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlazorEShop.Api.AspNet.Controllers
{
    [ApiController]
    [Route("api/v1/files")]
    public sealed class FilesController : ControllerBase
    {
        private readonly IImagesService _service;

        public FilesController(IImagesService service)
        {
            _service = service;
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<Guid> Upload(IFormFile file)
        {
            using var stream = file.OpenReadStream();
            return await _service.Upload(new File(stream), file.FileName, file.ContentType);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Download(Guid id)
        {
            var (file, image) = await _service.Download(id);
            return File(file.Data, image.ContentType, image.Name, true);
        }

        [HttpGet]
        public async Task<List<ImageModel>> GetAll()
        {
            var files = await _service.GetAll();
            return files.Select(f => new ImageModel(f)).ToList();
        }
    }
}
