﻿using BlazorEShop.Api.EF.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlazorEShop.Api.EF.Configs
{
    internal sealed class FileConfig : IEntityTypeConfiguration<ImageDto>
    {
        public void Configure(EntityTypeBuilder<ImageDto> builder)
        {
            builder.HasKey(b => b.Id);
            builder.HasIndex(f => new { f.Hash, f.Size }).IsUnique();
        }
    }
}
