﻿using BlazorEShop.Api.EF.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlazorEShop.Api.EF.Configs
{
    internal sealed class OrderConfig : IEntityTypeConfiguration<OrderDto>
    {
        public void Configure(EntityTypeBuilder<OrderDto> builder)
        {
            builder.HasKey(dto => dto.Id);
        }
    }
}
