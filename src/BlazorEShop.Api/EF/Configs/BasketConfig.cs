﻿using BlazorEShop.Api.EF.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlazorEShop.Api.EF.Configs
{
    internal sealed class BasketConfig : IEntityTypeConfiguration<BasketDto>
    {
        public void Configure(EntityTypeBuilder<BasketDto> builder)
        {
            builder.HasKey(b => b.Id);
        }
    }
}
