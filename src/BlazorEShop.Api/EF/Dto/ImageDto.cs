﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorEShop.ImagesModule.Entities;
using BlazorEShop.SharedModule.Entities;

namespace BlazorEShop.Api.EF.Dto
{
    public sealed class ImageDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public byte[] Hash { get; set; }
        public long Size { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public string ContentType { get; set; }

        public ImageInfo ToInfo() => new ImageInfo(Id, new FileHash(Hash), Size, Name, Created, ContentType);

        public ImageDto()
        {
            //For framework
        }

        public ImageDto(ImageInfo image)
        {
            Id = image.Id;
            Hash = image.Hash.Value;
            Size = image.Size;
            Name = image.Name;
            Created = image.Created;
            ContentType = image.ContentType;
        }
    }
}
