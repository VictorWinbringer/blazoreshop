﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorEShop.OrdersModule.Entities;

namespace BlazorEShop.OrdersModule.SecondaryPorts
{
    public interface IOrderRepository
    {
        void Add(Order order);
        Task<Order> Get(OrderId id);
        Task<List<Order>> Get(Client buyer);
        void Update(Order order);
    }
}
