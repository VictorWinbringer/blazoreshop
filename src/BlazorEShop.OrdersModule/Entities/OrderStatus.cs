﻿namespace BlazorEShop.OrdersModule.Entities
{
    public enum OrderStatus
    {
        Created = 10,
        Delivered,
    }
}
