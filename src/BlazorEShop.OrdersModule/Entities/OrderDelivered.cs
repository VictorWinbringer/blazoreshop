﻿using BlazorEShop.SharedModule.Entities;

namespace BlazorEShop.OrdersModule.Entities
{
    public sealed class OrderDelivered : OrderState
    {
        public OrderDelivered() : base(OrderStatus.Delivered)
        {

        }

        public override OrderState Delivered()
        {
            throw new ApiException(ApiExceptionCode.OrderAlreadyDelivered);
        }
    }
}
