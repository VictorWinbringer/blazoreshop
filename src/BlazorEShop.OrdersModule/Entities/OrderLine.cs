﻿using BlazorEShop.SharedModule.Entities;

namespace BlazorEShop.OrdersModule.Entities
{
    public class OrderLine : Line<OrderProduct>
    {
        public Order Order { get; }

        public OrderLine(OrderProduct orderProduct, uint quantity) : base(orderProduct, quantity)
        {
        }
    }
}
