﻿using System.ComponentModel.DataAnnotations;

namespace BlazorEShop.Sso.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
