﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using static IdentityModel.OidcConstants;
using GrantTypes = IdentityServer4.Models.GrantTypes;

namespace BlazorEShop.Sso.Configuration
{
    public class Config
    {
        // ApiResources define the apis in your system
        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
                new ApiResource("api", "BlazorEShop API"),
            };
        }

        // Identity resources are data like user ID, name, or email address of a user
        // see: http://docs.identityserver.io/en/release/configuration/resources.html
        public static IEnumerable<IdentityResource> GetResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResource("role", new string[]{ ClaimTypes.Role, JwtClaimTypes.Role})
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(Dictionary<string, string> clientsUrl)
        {
            return new List<Client>
            {
                    new Client
                {
                    ClientId = "spaBlazorClient",
                    ClientName = "SPA Blazor Client",
                    RequireClientSecret = false,
                    RequireConsent = false,
                    RedirectUris = new List<string>
                    {
                        $"{clientsUrl["SpaBlazor"]}/oidc/callbacks/authentication-redirect",
                        $"{clientsUrl["SpaBlazor"]}/authentication/login-callback",
                        $"{clientsUrl["SpaBlazor"]}/authentication/login-failed",
                        $"{clientsUrl["SpaBlazor"]}",
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        $"{clientsUrl["SpaBlazor"]}/oidc/callbacks/logout-redirect",
                        $"{clientsUrl["SpaBlazor"]}",
                    },
                    AllowedCorsOrigins = new List<string>
                    {
                        $"{clientsUrl["SpaBlazor"]}",
                    },
                    AllowedGrantTypes = GrantTypes.Code,
                    AllowedScopes = { "openid", "profile", "email", "api", "role" },
                    AllowOfflineAccess = true,
                    RefreshTokenUsage = TokenUsage.ReUse
                },
                new Client
                {
                    ClientId = "spaAngularClient",
                    ClientName = "SPA Angular Client",
                    AccessTokenType = AccessTokenType.Jwt,
                    RequireClientSecret = false,
                    RequireConsent = false,
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    AllowAccessTokensViaBrowser = true,
                    RedirectUris = new List<string>
                    {
                        $"{clientsUrl["SpaAngular"]}/callback",
                        $"{clientsUrl["SpaAngular"]}/silent-renew.html",
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        $"{clientsUrl["SpaAngular"]}/unauthorized",
                        $"{clientsUrl["SpaAngular"]}",
                    },
                    AllowedCorsOrigins = new List<string>
                    {
                        $"{clientsUrl["SpaAngular"]}",
                    },
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "api",
                        "role"
                    },
                },
                new Client
                {
                    ClientId = "apiSwaggerUi",
                    ClientName = "Api Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = { $"{clientsUrl["ApiSwagger"]}/oauth2-redirect.html" },
                    PostLogoutRedirectUris = { $"{clientsUrl["ApiSwagger"]}/" },
                    AllowedScopes =
                    {
                        "api",
                        "role"
                    }
                },
            };
        }
    }
}