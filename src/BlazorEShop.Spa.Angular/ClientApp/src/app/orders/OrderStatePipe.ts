import { Pipe, PipeTransform } from '@angular/core';
import { OrderStatus } from '../core/api/OrderStatus';
@Pipe({ name: 'orderState' })
export class OrderStatePipe implements PipeTransform {
    transform(value: OrderStatus, args?: any): string {
        switch (value) {
            case OrderStatus.Created:
                return 'Created';
            case OrderStatus.Delivered:
                return 'Delivered';
            default:
                throw new Error('Unown status ' + value);
        }
    }
}
