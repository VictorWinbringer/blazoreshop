import { OnInit, Component } from '@angular/core';
import { OrderService } from '../core/orders/OrdersService';
import { BasketService } from '../core/basket/BasketService';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LineModel } from '../core/api/LineModel';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html'
})
export class OrdersComponent implements OnInit {

    private _state = 'list';
    orderForm: FormGroup;
    constructor(private order: OrderService, private basket: BasketService, private route: ActivatedRoute) {
        this.orderForm = new FormGroup({
            'address': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(255)])
        });
    }

    ngOnInit(): void {
        this.route
            .paramMap
            .pipe(switchMap(params => params.getAll('action')))
            .subscribe(data => this.state = data);
        this.order.load();
        this.basket.load();
    }

    count(lines: LineModel[]) {
        return lines?.reduce((a, b) => a + b.quantity, 0) || 0;
    }

    orderSum(lines: LineModel[]) {
        return lines?.reduce((a, b) => a + (b.quantity * b.product.price), 0) || 0;
    }

    toCreateState() {
        this.state = 'create';
    }

    create() {
        if (this.orderForm.invalid || this.orderForm.untouched) {
            return;
        }
        const b = this.basket;
        this.order.onCreated = () => {
            b.load();
        };
        this.order.create(this.orderForm.value.address);
        this.state = 'list';
    }

    cancel() {
        this.state = 'list';
    }

    get canCreate() {
        return this.basket.itemsCount > 0;
    }
    get error() {
        return (this.order.error || ' ') + ' ' + (this.basket.error || ' ');
    }

    get orders() {
        return this.order.orders;
    }

    get sum() {
        return this.basket.model?.lines?.reduce((a, b) => a + (b.quantity * b.product.price), 0);
    }

    get state() {
        return this._state;
    }

    set state(value: string) {
        if (value) {
            switch (value) {
                case 'create':
                    this._state = value;
                    break;
                default:
                    this._state = 'list';
                    break;
            }
        } else {
            this._state = 'list';
        }
    }
}
