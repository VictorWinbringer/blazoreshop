import { Component, OnInit } from '@angular/core';
import { BasketService } from '../core/basket/BasketService';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html'
})
export class BasketComponent implements OnInit {

  columns: any[];

  constructor(public service: BasketService) {
    this.columns = [
      { field: 'product:title', header: 'Title' },
      { field: 'product:price', header: 'Price' },
      { field: 'quantity', header: 'Quantity' },
    ];
  }

  ngOnInit() {
    this.service.load();
  }
}
