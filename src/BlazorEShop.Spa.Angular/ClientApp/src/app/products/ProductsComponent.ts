import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api/lazyloadevent';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { FormValue } from '../core/products/FormValue';
import { ProductsService } from '../core/products/ProductsService';
import { ProductsFilter } from '../core/products/ProductsFilter';
import { BasketService } from '../core/basket/BasketService';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {

  columns: any[];
  lazyEvent: ProductsFilter;
  formValue: FormValue;
  filterForm: FormGroup;

  constructor(public service: ProductsService, public basketService: BasketService) {
    this.lazyEvent = { first: 0, rows: 10, sortField: 'id', sortOrder: 1 };
    this.formValue = { title: null, min: 0, max: Number.MAX_SAFE_INTEGER };
    this.filterForm = new FormGroup({
      'title': new FormControl(),
      'min': new FormControl(0, [Validators.min(0), Validators.required]),
      'max': new FormControl(null, this.minPriceValidator.bind(this))
    });
  }

  ngOnInit() {
    this.columns = [
      { field: 'title', header: 'Title' },
      { field: 'price', header: 'Price' },
    ];
    this.service.loadData(this.lazyEvent, this.formValue);
  }

  minPriceValidator(control: AbstractControl) {
    if (control?.value && this.filterForm?.value) {
      if (control.value < this.filterForm.value.min) {
        return { 'max': true };
      }
    }
    return null;
  }

  submit() {
    this.formValue = this.filterForm.value as FormValue;
    this.service.loadData(this.lazyEvent, this.formValue);
  }

  loadLazy(event: LazyLoadEvent) {
    this.lazyEvent = event as ProductsFilter;
    this.service.loadData(this.lazyEvent, this.formValue);
  }
}
