import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './AppComponent';
import { NavMenuComponent } from './nav-menu/NavMenuComponent';
import { HomeComponent } from './home/HomeComponent';
import { UnauthorizedComponent } from './unauthorized/UnauthorizedComponent';
import { CoreModule } from './core/CoreModule';
import { AuthGuard } from './core/auth/AuthGuard';
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorComponent } from './error/error.component';
import { ProductsComponent } from './products/ProductsComponent';
import { BasketComponent } from './basket/BasketComponent';
import { OrdersComponent } from './orders/OrdersComponent';
import { OrderStatePipe } from "./orders/OrderStatePipe";

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ProductsComponent,
    UnauthorizedComponent,
    ErrorComponent,
    BasketComponent,
    OrdersComponent,
    OrderStatePipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'basket', component: BasketComponent, canActivate: [AuthGuard] },
      { path: 'products', component: ProductsComponent },
      { path: 'unauthorized', component: UnauthorizedComponent },
      { path: 'forbidden', component: UnauthorizedComponent },
      { path: 'orders/:action', component: OrdersComponent, canActivate: [AuthGuard] },
      { path: '**', redirectTo: '' },
    ]),
    CoreModule,
    TableModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
