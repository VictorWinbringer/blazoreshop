import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService, UserInfo } from "../core/auth/AuthService";
import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BasketService } from '../core/basket/BasketService';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit, OnDestroy {

  isExpanded = false;
  private isAuthorizedSubscription: Subscription = new Subscription();
  private basketSub: Subscription = new Subscription();
  public isAuthorized = false;
  public userName: Observable<string>;
  totalItems: number;
  constructor(private authService: AuthService, private basketService: BasketService) { }

  ngOnInit(): void {
    this.isAuthorizedSubscription = this.authService.getIsAuthorized().subscribe(
      (isAuthorized: boolean) => {
        this.isAuthorized = isAuthorized;
        if (isAuthorized) {
          this.userName = this.authService.userInfo().pipe(map(x => x && x.name));
          this.basketService.load();
          this.totalItems = this.basketService.itemsCount;
        }
      });
    this.basketSub = this.basketService.basketLoaded.subscribe(x => this.totalItems = this.basketService.itemsCount);
  }

  ngOnDestroy(): void {
    this.isAuthorizedSubscription.unsubscribe();
    this.basketSub.unsubscribe();
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  public login() {
    this.authService.login();
  }

  public logout() {
    this.authService.logout();
  }
}
