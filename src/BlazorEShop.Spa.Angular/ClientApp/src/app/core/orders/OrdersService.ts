import { Injectable } from '@angular/core';
import { ApiService } from '../api/ApiService';
import { OrderModel } from '../api/OrderModel';
import { Subscription, Subscribable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class OrderService {

    orders: OrderModel[];
    error: string;
    onCreated: () => void;

    constructor(private api: ApiService) { }

    create(address: string) {
        this.api
            .createOrder(address)
            .subscribe(
                x => {
                    this.load();
                    this.onCreated?.call(this);
                },
                this.handle.bind(this)
            );
    }

    load() {
        this.api
            .getOrders()
            .subscribe(
                x => this.orders = x,
                this.handle.bind(this)
            );
    }

    handle(error: any) {
        console.error(error);
        this.error = JSON.stringify(error.error);
    }
}
