import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthModule, OidcSecurityService } from 'angular-auth-oidc-client';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ConfigurationService } from './config/ConfigurationService';
import { AuthService } from './auth/AuthService';
import { AuthGuard } from './auth/AuthGuard';
import { ApiService } from './api/ApiService';
import { AuthInterceptor } from './auth/AuthInterceptor';
import { ProductsService } from './products/ProductsService';
import { BasketService } from './basket/BasketService';

@NgModule({
  imports: [
    CommonModule,
    AuthModule.forRoot()
  ],
  declarations: [],
  providers: [
    AuthService,
    OidcSecurityService,
    AuthGuard,
    ConfigurationService,
    ApiService,
    ProductsService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    BasketService
  ]
})
export class CoreModule { }
