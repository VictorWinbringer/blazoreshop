import { Injectable } from '@angular/core';
import { ApiService } from '../api/ApiService';
import { ProductModel } from '../api/ProductModel';
import { BasketModel } from '../api/BasketModel';
import { Subscription, Observable, of, from, Subscriber } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class BasketService {

    error: string;
    model: BasketModel;
    basketLoaded: Observable<any>;
    constructor(private api: ApiService) {
        const bs = this;
        this.basketLoaded = new Observable(function subscribe(subscriber) {
            let count = 0;
            const intervalId = setInterval(() => {
                if (count !== bs.itemsCount) {
                    count = bs.itemsCount;
                    subscriber.next('hi');
                }
            }, 100);

            return function unsubscribe() {
                clearInterval(intervalId);
            };
        });
    }

    get itemsCount() {
        return this.model?.lines?.reduce<number>((a, b) => b.quantity + a, 0) || 0;
    }

    load() {
        this.api.getBasket()
            .subscribe(x => {
                this.model = x;
            }, this.handle.bind(this));
    }

    add(product: ProductModel) {
        this.api.addToBasket(product)
            .subscribe(x => {
                this.load();
            }, this.handle.bind(this));
    }

    remove(product: ProductModel) {
        this.api.removeFromBasket(product)
            .subscribe(x => {
                this.load();
            }, this.handle.bind(this));
    }

    clear() {
        this.api.clearBasket()
            .subscribe(
                x => this.load(),
                this.handle.bind(this)
            );
    }

    handle(error: any) {
        console.error(error);
        this.error = JSON.stringify(error.error);
    }
}
