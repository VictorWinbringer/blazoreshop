export interface IConfiguration {
  ssoUri: string;
  apiUri: string;
}
