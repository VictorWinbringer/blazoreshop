import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable, Subject, of } from 'rxjs';
import { IConfiguration } from "./IConfiguration"; import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ConfigurationService {

  private serverSettings: IConfiguration;
  private isReady: boolean = false;
  private baseURI: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseURI = baseUrl;
  }

  load(): Observable<IConfiguration> {
    if (this.isReady)
      return of<IConfiguration>(this.serverSettings);
    const baseURI = this.baseURI.endsWith('/') ? this.baseURI : `${this.baseURI}/`;
    let url = `${baseURI}api/v1/config`;
    return this.http.get(url).pipe(map((response) => {
      console.log('server settings loaded');
      this.serverSettings = response as IConfiguration;
      console.log(this.serverSettings);
      this.isReady = true;
      return this.serverSettings;
    }));
  }
}
