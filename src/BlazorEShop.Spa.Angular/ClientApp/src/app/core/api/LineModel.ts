import { ProductModel } from './ProductModel';
export interface LineModel {
  quantity: number;
  product: ProductModel;
}
