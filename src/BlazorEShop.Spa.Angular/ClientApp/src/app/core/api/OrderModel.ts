import { OrderStatus } from './OrderStatus';
import { LineModel } from './LineModel';
export interface OrderModel {
  id: string;
  buyer: string;
  status: OrderStatus;
  lines: LineModel[];
  address: string;
}
