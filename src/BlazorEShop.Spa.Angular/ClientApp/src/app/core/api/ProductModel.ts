export interface ProductModel {
  id: string;
  version: string;
  title: string;
  price: number;
  imageId: string;
}
