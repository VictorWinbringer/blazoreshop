export interface PageResultModel<T> {
  totalCount: number;
  value: T[];
}
