import { LineModel } from './LineModel';
export interface BasketModel {
  lines: LineModel[];
}
