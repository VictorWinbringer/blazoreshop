import { ProductOderBy } from './ProductOderBy';
export interface ProductsFilterModel {
  skip: number;
  take: number;
  title: string | null;
  minPrice: number;
  maxPrice: number;
  descending: boolean;
  orderBy: ProductOderBy;
}
