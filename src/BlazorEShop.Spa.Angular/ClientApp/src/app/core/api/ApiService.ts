import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../config/ConfigurationService';
import { IConfiguration } from '../config/IConfiguration';
import { Observable } from 'rxjs';
import { map, concatAll } from 'rxjs/operators';
import { OrderModel } from './OrderModel';
import { BasketModel } from './BasketModel';
import { PageResultModel } from './PageResultModel';
import { ProductModel } from './ProductModel';
import { ProductsFilterModel } from './ProductsFilterModel';

@Injectable({ providedIn: 'root' })
export class ApiService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  public getFiltered(model: ProductsFilterModel): Observable<PageResultModel<ProductModel>> {
    return this.post<PageResultModel<ProductModel>>('/products/filter', model);
  }

  public addToBasket(product: ProductModel): Observable<number> {
    return this.post<number>('/basket/lines/add', product);
  }

  public removeFromBasket(product: ProductModel): Observable<number> {
    return this.post<number>(`/basket/lines/${product.id}/remove`, '');
  }

  public getBasket(): Observable<BasketModel> {
    return this.get<BasketModel>('/basket');
  }

  public getOrders(): Observable<OrderModel[]> {
    return this.get<OrderModel[]>('/orders');
  }

  public createOrder(address: string): Observable<string> {
    return this.post<string>('/orders/create', { address });
  }

  public clearBasket(): Observable<number> {
    return this.delete<number>('/basket/lines');
  }

  private post<T>(url: string, model: any): Observable<T> {
    return this.configService.load().pipe(map(x => {
      const uri = this.getFullUrl(x, url);
      return this.http.post<T>(uri, model);
    }), concatAll());
  }

  private get<T>(url: string): Observable<T> {
    return this.configService.load().pipe(map(x => {
      const uri = this.getFullUrl(x, url);
      return this.http.get<T>(uri);
    }), concatAll());
  }

  private delete<T>(url: string): Observable<T> {
    return this.configService.load().pipe(map(x => {
      const uri = this.getFullUrl(x, url);
      return this.http.delete<T>(uri);
    }), concatAll());
  }

  private getFullUrl(config: IConfiguration, path: string): string {
    let uri: string = config.apiUri;
    uri = uri.endsWith('/') ? uri : uri + '/';
    path = path.startsWith('/') ? path : '/' + path;
    return `${uri}api/v1${path}`;
  }

  public getApiUrl(url: string): Observable<string> {
    return this.configService.load().pipe(map(x => {
      const uri = this.getFullUrl(x, url);
      return uri;
    }));
  }
}
