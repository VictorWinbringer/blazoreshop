export interface FormValue {
  title: string;
  min: number;
  max: number | null;
}
