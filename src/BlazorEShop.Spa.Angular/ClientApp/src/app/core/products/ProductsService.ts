import { ApiService } from '../api/ApiService';
import { ProductOderBy } from '../api/ProductOderBy';
import { ProductModel } from '../api/ProductModel';
import { PageResultModel } from '../api/PageResultModel';
import { FormValue } from './FormValue';
import { AuthService } from '../auth/AuthService';
import { Injectable } from '@angular/core';
import { ProductsFilter } from './ProductsFilter';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ProductsService {
  products: PageResultModel<ProductModel>;
  errors: string;
  totalCount: number;
  isLoading: boolean;
  isAuthorized: boolean;

  constructor(private api: ApiService, private authService: AuthService) { }

  getImageUrl(product: ProductModel): Observable<string> {
    return this.api.getApiUrl(`files/${product.imageId}`);
  }

  handle(error: any) {
    this.isLoading = false;
    console.error(error);
    this.errors = JSON.stringify(error.error);
  }

  loadData(lazyEvent: ProductsFilter, formValue: FormValue) {
    let order = ProductOderBy.Id;
    if (lazyEvent.sortField) {
      switch (lazyEvent.sortField) {
        case 'title':
          order = ProductOderBy.Name;
          break;
        case 'price':
          order = ProductOderBy.Price;
          break;
        default:
          break;
      }
    }
    this.isLoading = true;
    this.api.getFiltered({
      title: formValue.title,
      skip: lazyEvent.first,
      take: lazyEvent.rows,
      minPrice: formValue.min,
      maxPrice: formValue.max || Number.MAX_SAFE_INTEGER,
      descending: lazyEvent.sortOrder === -1,
      orderBy: order
    })
      .subscribe((x) => {
        this.products = x;
        this.errors = null;
        this.isLoading = false;
      }, this.handle.bind(this));
  }

  auth() {
    this.authService.getIsAuthorized().subscribe(
      (isAuthorized: boolean) => {
        this.isAuthorized = isAuthorized;
      });
  }
}
