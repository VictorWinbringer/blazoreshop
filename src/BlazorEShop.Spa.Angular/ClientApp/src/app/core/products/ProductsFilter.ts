export interface ProductsFilter {
  first: number;
  rows: number;
  sortField: string;
  sortOrder: number;
}
