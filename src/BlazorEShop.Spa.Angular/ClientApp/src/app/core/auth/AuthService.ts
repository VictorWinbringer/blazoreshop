import { Injectable, OnDestroy, Inject } from '@angular/core';
import { OidcSecurityService, OpenIdConfiguration, AuthWellKnownEndpoints, AuthorizationResult, AuthorizationState } from 'angular-auth-oidc-client';
import { Observable, Subscription } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ConfigurationService } from '../config/ConfigurationService';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService implements OnDestroy {

  isAuthorized = false;
  authUrl = '';

  constructor(
    private oidcSecurityService: OidcSecurityService,
    private router: Router,
    @Inject('BASE_URL') private originUrl: string,
    private config: ConfigurationService
  ) {
  }

  private isAuthorizedSubscription: Subscription = new Subscription;

  ngOnDestroy(): void {
    if (this.isAuthorizedSubscription) {
      this.isAuthorizedSubscription.unsubscribe();
    }
  }

  public initAuth() {
    this.config.load().subscribe(x => {
      this.authUrl = x.ssoUri;
      this.initAuthInner();
    });
  }

  private initAuthInner() {
    const openIdConfiguration: OpenIdConfiguration = {
      stsServer: this.authUrl,
      redirect_url: this.originUrl + 'callback',
      client_id: 'spaAngularClient',
      response_type: 'code',
      scope: 'openid profile api',
      post_logout_redirect_uri: this.originUrl,
      forbidden_route: '/forbidden',
      unauthorized_route: '/unauthorized',
      silent_renew: true,
      silent_renew_url: this.originUrl + 'silent-renew.html',
      history_cleanup_off: true,
      auto_userinfo: true,
      log_console_warning_active: true,
      log_console_debug_active: true,
      max_id_token_iat_offset_allowed_in_seconds: 10,
      silent_renew_offset_in_seconds: 10,
    };

    const authWellKnownEndpoints: AuthWellKnownEndpoints = {
      issuer: this.authUrl,
      jwks_uri: this.authUrl + '/.well-known/openid-configuration/jwks',
      authorization_endpoint: this.authUrl + '/connect/authorize',
      token_endpoint: this.authUrl + '/connect/token',
      userinfo_endpoint: this.authUrl + '/connect/userinfo',
      end_session_endpoint: this.authUrl + '/connect/endsession',
      check_session_iframe: this.authUrl + '/connect/checksession',
      revocation_endpoint: this.authUrl + '/connect/revocation',
      introspection_endpoint: this.authUrl + '/connect/introspect',
    };

    this.oidcSecurityService.setupModule(openIdConfiguration, authWellKnownEndpoints);

    if (this.oidcSecurityService.moduleSetup) {
      this.doCallbackLogicIfRequired();
    } else {
      this.oidcSecurityService.onModuleSetup.subscribe(() => {
        this.doCallbackLogicIfRequired();
      });
    }
    this.isAuthorizedSubscription = this.oidcSecurityService.getIsAuthorized().subscribe((isAuthorized => {
      this.isAuthorized = isAuthorized;
    }));

    this.oidcSecurityService.onAuthorizationResult.pipe<AuthorizationResult>(map(
      (authorizationResult: AuthorizationResult) => {
        this.onAuthorizationResultComplete(authorizationResult);
        return authorizationResult;
      })).subscribe(x => { console.log(x); }, x => { console.log(x); }, () => { console.log('COMPLITE onAuthorizationResult'); });
  }

  private onAuthorizationResultComplete(authorizationResult: AuthorizationResult) {

    console.log('AUTH RESULT RECEIVED AUTHORIZATIONSTATE:'
      + authorizationResult.authorizationState
      + ' VALIDATIONRESULT:' + authorizationResult.validationResult);

    if (authorizationResult.authorizationState === AuthorizationState.unauthorized) {
      if (this.router.url === '/') {
        return;
      }
      if (window.parent) {
        // sent from the child iframe, for example the silent renew
        this.router.navigate(['/unauthorized']);
      } else {
        window.location.href = '/unauthorized';
      }
    }
  }

  private doCallbackLogicIfRequired() {
    this.oidcSecurityService.authorizedCallbackWithCode(window.location.toString());
  }

  getIsAuthorized(): Observable<boolean> {
    return this.oidcSecurityService.getIsAuthorized();
  }

  login() {
    console.log('start login');
    this.oidcSecurityService.authorize();
  }

  logout() {
    console.log('start logoff');
    this.oidcSecurityService.logoff();
  }

  userInfo(): Observable<UserInfo> {
    return this.oidcSecurityService.getUserData<UserInfo>();
  }
}

export interface UserInfo {
  sub: string;
  name: string;
  role: string[];
}
