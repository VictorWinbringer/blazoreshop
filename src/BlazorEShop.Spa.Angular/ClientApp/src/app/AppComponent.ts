import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './core/auth/AuthService';
import { ConfigurationService } from './core/config/ConfigurationService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'app';
  constructor(private authService: AuthService, private configService: ConfigurationService) {

  }
  ngOnInit(): void {
    this.configService.load();
    this.authService.initAuth();
  }

  ngOnDestroy() {
    this.authService.ngOnDestroy();
  }
}
