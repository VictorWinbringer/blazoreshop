﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorEShop.ImagesModule.Entities;
using BlazorEShop.ImagesModule.PrimaryPorts;
using BlazorEShop.ImagesModule.SecondaryPorts;
using BlazorEShop.SharedModule.Entities;
using BlazorEShop.SharedModule.SecondaryPorts;

namespace BlazorEShop.ImagesModule.PrimaryAdapters
{
    public class ImagesService : IImagesService
    {
        private readonly IFilesRepository _filesRepository;
        private readonly IImagesRepository _imagesRepository;
        private readonly IImagesUow _uow;

        public ImagesService(IFilesRepository filesRepository, IImagesRepository imagesRepository, IImagesUow uow)
        {
            _filesRepository = filesRepository;
            this._imagesRepository = imagesRepository;
            _uow = uow;
        }

        public Task<List<ImageInfo>> GetAll()
        {
            return _imagesRepository.GetAll();
        }

        public async Task<Guid> Upload(File file, string name, string contentType)
        {
            var hash = await _filesRepository.Upload(file);
            var old = await _imagesRepository.Find(hash);
            if (old != default)
                return old;
            if (!file.IsImage())
                throw new ApiException(ApiExceptionCode.FileIsNotImage);
            var image = new ImageInfo(file, name, contentType);
            await _imagesRepository.Add(image);
            await _uow.SaveAsync();
            return image.Id;
        }

        public async Task<(File, ImageInfo)> Download(Guid id)
        {
            var image = await _imagesRepository.Get(id);
            var file = await _filesRepository.Download(image.Hash);
            return (file, image);
        }
    }
}
