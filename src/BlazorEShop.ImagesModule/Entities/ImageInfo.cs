﻿using System;
using BlazorEShop.SharedModule.Entities;
using FluentValidationGuard;

namespace BlazorEShop.ImagesModule.Entities
{
    public class ImageInfo
    {
        public Guid Id { get; }
        public FileHash Hash { get; }
        public long Size { get; }
        public string Name { get; }
        public DateTime Created { get; }
        public string ContentType { get; }

        public ImageInfo(File file, string name, string contentType) : this(file.ComputeFileHash(), file.Size, name, contentType) { }

        public ImageInfo(
            FileHash hash,
            long size,
            string name,
            string contentType
            ) : this(
            Guid.NewGuid(),
            hash,
            size,
            name,
            DateTime.UtcNow,
            contentType
            )
        {

        }

        public ImageInfo(Guid id, FileHash hash, long size, string name, DateTime created, string contentType)
        {
            Validator.Begin(id, nameof(id))
                .NotDefault()
                .Map(hash, nameof(hash))
                .NotDefault()
                .Map(hash.Value, "hash value")
                .NotNull()
                .NotEmpty()
                .Map(size, nameof(size))
                .IsGreater(0)
                .Map(name, nameof(name))
                .NotNull()
                .NotWhiteSpace()
                .Map(created, nameof(created))
                .NotDefault()
                .IsLess(DateTime.UtcNow.AddDays(1))
                .Map(contentType, nameof(contentType))
                .NotNull()
                .NotWhiteSpace()
                .ThrowApiException(nameof(File), nameof(File));
            Id = id;
            Hash = hash;
            Size = size;
            Name = name;
            Created = created;
            ContentType = contentType;
        }
    }
}
