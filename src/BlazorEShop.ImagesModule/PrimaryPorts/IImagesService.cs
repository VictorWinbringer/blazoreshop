﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorEShop.ImagesModule.Entities;
using BlazorEShop.SharedModule.Entities;

namespace BlazorEShop.ImagesModule.PrimaryPorts
{
    public interface IImagesService
    {
        Task<List<ImageInfo>> GetAll();
        Task<Guid> Upload(File file, string name, string contentType);
        Task<(File, ImageInfo)> Download(Guid id);
    }
}
