﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorEShop.ImagesModule.Entities;
using BlazorEShop.SharedModule.Entities;

namespace BlazorEShop.ImagesModule.SecondaryPorts
{
    public interface IImagesRepository
    {
        Task<ImageInfo> Get(Guid id);
        Task<Guid> Find(FileHash hash);
        Task<List<ImageInfo>> GetAll();
        Task Add(ImageInfo file);
    }
}
